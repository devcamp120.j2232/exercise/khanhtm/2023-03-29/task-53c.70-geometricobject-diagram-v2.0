import model.Circle;
import model.ResizableCircle;

public class Task53C70 {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(1.0);
        ResizableCircle resizablecircle = new ResizableCircle(5.0);
        System.out.println("Hinh tron 1: " + circle);
        System.out.println("Hinh tron 2: " + resizablecircle);
        System.out.println("Dien tich hinh tron 1: " + circle.getArea());
        System.out.println("Chu vi hinh tron 1: " + circle.getPerimeter()); 
        System.out.println("Dien tich hinh tron 2 ban dau: " + resizablecircle.getArea());
        System.out.println("Chu vi hinh tron 2 ban dau: " + resizablecircle.getPerimeter());
        resizablecircle.resize(50);
        System.out.println("Dien tich hinh tron 2 resize: " + resizablecircle.getArea());
        System.out.println("Chu vi hinh tron 2 resize: " + resizablecircle.getPerimeter());
    }
}
